

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="ctl00_Head1"><title>
	Forgotten Password
</title><link type="text/css" href="styles/reset.2012072612.css" rel="Stylesheet" /><link type="text/css" href="styles/textburst.2012072612.css" rel="Stylesheet" /><link type="text/css" href="styles/content.2012072612.css" rel="Stylesheet" /><link type="text/css" href="scripts/colorbox/colorbox.2012072612.css.htm" rel="Stylesheet" /><link type="text/css" href="scripts/ui/smoothness/jquery-ui-1.8.6.min.2012072612.css.htm" rel="Stylesheet" />
    <!--[if lte IE 6]>
        <link href="/styles/textburst-ie6.2012072612.css" rel="stylesheet" type="text/css" /><link href="/styles/content-ie6.2012072612.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <!--[if IE 8]>
        <link href="/styles/textburst-ie8.2012072612.css" rel="stylesheet" type="text/css" /><link href="/styles/content-ie8.2012072612.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1585897-2']);
        _gaq.push(['_trackPageview']);
    </script>
        
    </head>
<body>
    <form method="post" action="forgotten1.php" id="aspnetForm">
      <div id="loggedout">
	
      <div id="loggedout-content">
            <div class="column">
                
    <div id="ctl00_LoggedOutContent_ForgottenPanel">
		
        <h1>Forgot your password?</h1>
        <fieldset>
        
            <div class="field">
                <label for="ctl00_LoggedOutContent_Username" id="ctl00_LoggedOutContent_UsernameLabel">Username</label><span class="info">Required</span>
                <input name="username" type="text" id="username" />
                
            </div>
            <div class="field">
                <input type="submit" name="submit" value="Send Link" id="submit" class="button" />
            </div>
            <div class="field">
            <a id="ctl00_LoggedOutContent_LoginPage" href="login.php">Log in here</a>
        </div>
        </fieldset>
    
	</div>
    

            </div>
            <div class="column column-two">
                <div class="blurb">
                   
    <div id="ctl00_LoggedOutBlurb_ForgottenBlurb">
		
        <h3>How this works...</h3>
        <p>Fill in your username and we'll email you a password </p>
        <p>Log in with the new password and change it to somethng more memorable</p>
    
	</div>
    

                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                </div>
            </div>
        </div>
    
</div>
    </form>
    <script src="scripts/jquery-1.6.1.js" type="text/javascript"></script>  
    <script src="scripts/common.js" type="text/javascript"></script>  
    <script src="scripts/colorbox/jquery.colorbox.js" type="text/javascript"></script>
    <script src="scripts/ui/jquery-ui-1.8.6.js" type="text/javascript"></script>
    <script src="scripts/jquery.timer.js" type="text/javascript"></script>
    
    
    
    <!--[if lte IE 7]>  
        <script type="text/javascript" src="/scripts/DD_roundies_0.0.2a-min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                DD_roundies.addRule('#wrapper, #loggedout, .section', '10px');                                    
            });
        </script>
    <![endif]-->
    <!--[if lte IE 6]>
        <script type="text/javascript">
            $(document).ready(function() {
                DD_roundies.addRule('#header, #loggedoutheader');
            });            
        </script>
    <![endif]-->
    <script type="text/javascript">
        $(document).ready(function () {
            $.timer(10000, function (timer) {
                timer.reset(60000);
                $.ajax({
                    url: "/ajax.asmx/GetNotify",
                    success: function (data) {
                        if (data.d != null) {
                            $("#related .alert").remove();
                            $("#related").prepend('<div class="alert"><a href="#" class="ok" notifyid="' + data.d[0] + '">ok</a>' + data.d[1] + '</div>');
                            $("a.ok").click(function () {
                                $.ajax({
                                    url: "/ajax.asmx/DeleteNotify",
                                    data: "{ notificationId: \"" + $(this).attr("notifyid") + "\" }",
                                    cache: false
                                });
                                $("#related .alert").remove();
                                return false;
                            });
                        }
                    },
                    cache: false
                });
            });
        });
    </script>
    <script type="text/javascript">
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
        })();
    </script>

</body>
</html>
            });
                                $("#related .alert").remove();
                                return false;
                            });
                        }
                    },
                    cache: false
                });
            });
        });
    </script>
    <script type="text/javascript">
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
        })();
    </script>

</body>
</html>
