<?php

session_start();
include "./config/auth.inc.php";
include "./config/conn.inc.php";
$d = date('l jS \of F Y h:i A');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="styles/textburst.2012072612.css"/>


<title>New News Item</title>
</head>

<body>
<p>Add News Article</p>
<form id="form1" name="form1" method="post" action="savenews.php">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>News Title (short)</td>
      <td><label for="byline"></label>
      <input type="text" name="byline" id="byline" /></td>
    </tr>
    <tr>
      <td>Article Body( long)</td>
      <td><label for="article"></label>
      <textarea name="article" id="article" cols="45" rows="5"></textarea></td>
    </tr>
    <tr>
      <td><input name="date" type="hidden" id="date" value="<?php echo $d ?>" />
      <input name="publish" type="hidden" id="publish" value="1" /></td>
      <td><input name="button" type="submit" class="button" id="button" value="Submit" /></td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
</body>
</html>