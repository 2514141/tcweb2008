<?php

session_start();
include "./config/auth.inc.php";
include "./config/conn.inc.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TextCentre</title>
<meta name="keywords" content="Specific Template, HTML CSS, free website template" />
<meta name="description" content="Specific Template is a free website template provided by TemplateMo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>
</head>
<body>
<div id="templatemo_header_wrapper">
	<div id="templatemo_header">

    	<div id="title">TextCentre
            <span>Administration</span>
        </div>
        
        <div id="twitter">
          <h1>0/
<?php
 $query = "SELECT SUM(Balance) FROM credits"; 
	 
$result = mysql_query($query) or die(mysql_error());

// Print out result
while($row = mysql_fetch_array($result)){
	echo $row['SUM(Balance)'];
}
?>
          </h1>
          <h4>Total System Credits</h4>
        </div>
	</div> <!-- end of header -->
    
</div> <!-- end of header wrapper -->

<div id="templatemo_menu_wrapper">

    <div id="templatemo_menu">
        <ul>
            <li><a href="#"><span></span>Home</a></li>
            <li><a href="#"><span></span>New Message</a></li>
            <li><a href="#"><span></span>Inbox 0</a></li>
            <li><a href="#"><span></span>Contacts</a></li>   <li><a href="#"><span></span> Account</a></li>
 			<li><a href="#"><span></span>Add Credit</a></li>
            <li><a href="logout.php"><span></span>Log Out</a></li>
        </ul>   
	</div> <!-- end of menu -->
</div> <!-- end of menu wrapper -->

<div id="templatemo_content_wrapper">

	<div id="templatemo_content">
	  <div class="section_w620 fl margin_right_50">
	    <div class="header_01"> Welcome Username</div>
	    <p class="em_text">Changes made here will edit what customers can see on there panels</p>
	    <div class="margin_bottom_40"></div>
	    <div class="section_320 fl margin_right_40">
	      <div class="header_01"> Send Messages	      </div>
	      <ul class="list_01">
	        <li>Compose a new message</li>
	        <li>Compise a message from template</li>
          </ul>
        </div>
	    <div class="section_w250_without_padding fl">
	      <div class="header_01"> Inbox</div>
	      <p>Inbox contents generated</p>
        </div>
	    <div class="cleaner"></div>
      </div>
	  <div class="section_w250 fr">
        
        	<div class="section_w250_title news_title">
            	Our News<a class="cboxElement" href="addnews.php">  
                        New
                        </a>
                        </div>
       	<div class="w250_content">
        	  <?php
$queryn = "SELECT * " .
			"FROM news " . 
			"WHERE published = '1'" .
			"ORDER BY newsid DESC LIMIT 0, 4";
			$resultsn = mysql_query($queryn) or die(mysql_error());
	
while ($rown = mysql_fetch_array($resultsn)) {
	?>
        	  <div class="latest_news">
        	    <div class="header_04"><?php echo $rown['date'] ?><a href="#"> Edit</a> <a href="unpublish.php?article=<?php echo $rown['newsid'] ?>">UnPublish</a></div>
        	    <div class="header_02"><a href="#"><?php echo $rown['byline'] ?></a></div>
        	    <p><?php echo $rown['Article'] ?><a href="#"></a></p>
      	    </div>
       	  <div class="margin_bottom_20"></div>
       	  <?php } ?>
   	      <a href="#">New Article</a></div>
        	<div class="margin_bottom_20"></div>
<?php  
$query = "SELECT * " .
			"FROM serverstatus " . 
			"WHERE Active = '1'
			Limit 1";
			$results = mysql_query($query) or die(mysql_error());
	
while ($row = mysql_fetch_array($results)) {?>
        <div class="section_w250_title testimonial_title">

        	
            	Service Level  </div> 
            
            <div class="w250_content">
      
                <div class="header_02"><a href="#"><?php echo $row['Level'] ?> </a></div>
                <p><?php echo $row['Description'] ?></p>
	<?php
}
?>
            </div>
        <p>Change Service Level: <br />
              <a href="serverstatus.php?status=1">OK<br />
              </a><a href="serverstatus.php?status=2">BUSY</a><br /> 
              <a href="serverstatus.php?status=3">DOWN</a>
            <br />
            <a href="serverstatus.php?status=4">Maintenance UNPLANED</a><br />
            
          <a href="serverstatus.php?status=5">Maintenance Planned</a></p>
            <p><a href="serverstatus.php?status=6">Online (Under Test)</a></p>
            <p><a href="serverstatus.php?status=7">Offline (Under Test)</a></p>
        <div class="margin_bottom_20"></div>
        
      </div>
        
        <div class="margin_bottom_20"></div>
    </div> <!-- end of content -->

</div> <!-- end of content wrapper -->

<div id="templatemo_footer_wrapper">

	<div id="templatemo_footer">
    
		Copyright © 2012 
	    <div class="margin_bottom_20"></div>
	</div> <!-- end of footer -->
</div> <!-- end of footer wrapper -->
    
</body>
</html>